# Proxy
`proxy` manages kernel routes and neighbour proxy used by an arp or ndp proxy. A
sample configuration file is provided in `proxy.example.conf`.
