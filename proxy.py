#!/usr/bin/python3

import json
import subprocess
import os
import argparse
import ipaddress
import ldap


path = os.path.dirname(os.path.abspath(__file__))

def route_handler(protocol, selector, routes, alter=False, dry=False):
	"""Manage kernel routes used by the proxy."""
	# Listing routes currently in the kernel
	c_routes = { route["dst"]: route["dev"] for route in json.loads(
			subprocess.run(["ip", selector, "-j", "route", "show", "protocol", protocol],
				capture_output=True
			).stdout or '[]')
	}

	# Adding new routes and fixing old routes
	for target, interface in routes.items():
		if target in c_routes and interface == c_routes[target]:
			continue
		elif target in c_routes:
			if alter:
				command = ["ip", "route", "change", target, "dev", interface, "protocol", protocol]
				if dry:
					print(*command)
				else:
					subprocess.run(command)
		else:
			command = ["ip", "route", "add", target, "dev", interface, "protocol", protocol]
			if dry:
				print(*command)
			else:
				subprocess.run(command)

	# Flushing old routes
	for target,interface in c_routes.items():
		if target not in routes and alter:
			command = ["ip", "route", "del", target, "dev", interface, "protocol", protocol]
			if dry:
				print(*command)
			else:
				subprocess.run(command)


def proxy_handlers(protocol, proxies, alter=False, dry=False):
	"""Manage neighboor proxy used by the proxy."""
	# Listing current proxies
	c_proxies = { (proxy["dst"], proxy["dev"]) for proxy in json.loads(
			subprocess.run(["ip", "-j", "-6", "neigh", "show", "proxy", "protocol", protocol],
				capture_output=True
			).stdout  or '[]' )
	}

	# Adding new proxies
	for target, interfaces in proxies.items():
		for interface in interfaces:
			if (target, interface) not in c_proxies:
				command = ["ip", "neigh", "add", "proxy", target, "dev", interface, "protocol", protocol]
				if dry:
					print(*command)
				else:
					subprocess.run(command)

	# Flushing old proxies
	for target, interface in c_proxies:
		if (target not in proxies or interface not in proxies[target]) and alter:
			command = ["ip", "neigh", "del", "proxy", target, "dev", interface, "protocol", protocol]
			if dry:
				print(*command)
			else:
				subprocess.run(command)


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Proxy ARP route and neighbour inserter")
	parser.add_argument('-c', '--config', help='Path to the configuration file [{}]'.format(os.path.join(path,'proxy.json')),
		default=os.path.join(path,'proxy.json')
	)
	parser.add_argument('-a', '--alter', help='Alter routes already present in the routing table', action='store_true')
	parser.add_argument('-p', '--protocol', help='Protocol to add the routes as')
	parser.add_argument('-d', '--dry', help='dry run', action='store_true')
	args = parser.parse_args()

	with open(args.config) as file:
		config = json.load(file)
	protocol = config["protocol"] if args.protocol is None else args.protocol

	hosts = None
	exception = None
	for server in config['ldap']:
		try:
			base = ldap.initialize(server['server'])
			if server['server'].startswith('ldaps://'):
				# On ne vérifie pas le certificat pour le LDAPS
				base.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_ALLOW)
				base.set_option(ldap.OPT_X_TLS_NEWCTX, 0)
			hosts = base.search_s("ou=hosts,dc=crans,dc=org", ldap.SCOPE_SUBTREE, "objectClass=ipHost")
		except ldap.SERVER_DOWN as e:
			exception = e
			continue
		break
	if hosts is None:
		raise exception


	hosts = [ {
			key: [ value.decode('utf-8') for value in values ] for key,values in host.items()
		} for _,host in hosts if any(cn.decode('utf-8').endswith(config['filter']) for cn in host["cn"])
	]

	arp_routes = {}
	ndp_routes = {}
	ndp_proxies = {}
	interfaces = set(config['proxy'].values())
	c_interfaces = { interface['ifname'] for interface in json.loads(
			subprocess.run(["ip", "-j", "link", "show" ],
				capture_output=True
			).stdout  or '[]' )
	}

	for host in hosts:
		ips = host["ipHostNumber"]
		if 'l' in host and host['l'][0] in config['proxy']:
			interface = config['proxy'][host['l'][0]]
		else:
			interface = config['proxy']['default']
		if interface not in c_interfaces:
			continue
		for ip in ips:
			if ipaddress.ip_address(ip).version == 4:
				arp_routes[ip] = interface
			else:
				ndp_routes[ip] = interface
				ndp_proxies[ip] = set.intersection(interfaces, c_interfaces) - {interface}

	route_handler(protocol, '-4', arp_routes, alter=args.alter, dry=args.dry)
	route_handler(protocol, '-6', ndp_routes, alter=args.alter, dry=args.dry)
	proxy_handlers(protocol, ndp_proxies, alter=args.alter, dry=args.dry)
